# Groepszoeker -- A Library for Finding Sub-Populations

Sometimes you need to look for a sub-population within a group whose values
add up to a given target value. That's what `groepszoeker` is for.

`groepszoeker.find_populations()` will return the largest sub-population
(i.e. the one that contains the most individual values) that it finds.

For example, looking for a target value of 500 in a list of numbers:

```python
>>> from groepszoeker import SearchableItem, find_population
>>> numbers = [100.0, 500.0, 50.0, 50, 50, 50, 200]
>>> population = find_population(
...     target=500,
...     tolerance=0,
...     items=[SearchableItem(value=number) for number in numbers],
... )
>>> 
>>> print(population.total_value)
500.0
>>> print("\n".join([str(item.value) for item in population.matched_items]))
200
50
50
50.0
100.0
50
```

## Installation

You can install with pip:

```python
pip install groepszoeker
```

## Searchable Items

The example above wraps the values in the population being searched in
`SearchableItem` instances. This is to give each item its own identity; since
`groepszoeker` uses Python sets a lot, it can't simply have numbers passed
into it, otherwise duplicates will get removed. `SearchableItem` merely adds
a `UUID4` `id` field to the value so that nothing gets removed during searches.

However, you can pass an iterator of any uniquely identifiable objects in to
`find_populations()`. Provided that each object has a `value` attribute,
`groepszoeker` doesn't care.

This would allow you to do something like this with a SqlAlchemy model:

```python

class MyModel(Base):

    id = Column(Integer, primary_key=True)
    value = Column(Numeric, nullable=False)


items = session.query(MyModel).all()
largest_population = find_populations(target=1000, tolerance=500, items=items)
```

You could even, in a SqlAlchemy world, do something like the following, if
the field names don't quite line up with `groepszoeker`'s needs.

```python
class MyModel(Base):

    id = Column(Integer, primary_key=True)
    amount = Column(Numeric, nullable=False)

items = session.query([MyModel.id, MyModel.amount.label("value")).all()
largest_population = find_populations(target=1000, tolerance=500, items=items)
```
