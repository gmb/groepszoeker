# Copyright 2021 Graham Binns <hello@gmb.dev>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""Tests for groepszoeker."""

import pytest

import groepszoeker
from groepszoeker import (
    ClassifiedItemCollection,
    SearchableItem,
    SubPopulation,
    TotalBoundsCheckStatus,
    check_bounds,
    find_population,
)


class TestFindPopulation:
    def test_find_population_finds_exact_set_with_no_tolerance(self):
        items = [
            SearchableItem(value=500),
            SearchableItem(value=100),
            SearchableItem(value=900),
            SearchableItem(value=300),
            SearchableItem(value=700),
        ]
        result = find_population(target=900, tolerance=0, items=items)
        assert isinstance(result, SubPopulation)
        assert result.matched_items == set([items[0], items[1], items[3]])
        assert result.unmatched_items == set([items[2], items[-1]])
        assert result.total_value == 900

    def test_find_population_finds_optimal_set_with_tolerance(self):
        target = 1000
        tolerance = 100
        values = [
            SearchableItem(value=1),
            SearchableItem(value=3),
            SearchableItem(value=5),
            SearchableItem(value=7),
            SearchableItem(value=9),
            SearchableItem(value=100),
            SearchableItem(value=300),
            SearchableItem(value=500),
            SearchableItem(value=700),
            SearchableItem(value=900),
        ]

        result = find_population(target, tolerance, values)
        assert result.total_value == 925
        assert result.matched_items == set(values[0:8])
        assert (
            sum([item.value for item in result.matched_items])
            == result.total_value
        )

    def test_finds_optimal_set_with_noise(self):
        items = [
            SearchableItem(value=100),
            SearchableItem(value=300),
            SearchableItem(value=1),
            SearchableItem(value=500),
            SearchableItem(value=700),
            SearchableItem(value=901),
        ]
        result = find_population(target=900, tolerance=0, items=items)
        assert result.matched_items == set([items[0], items[1], items[3]])

    def test_find_population_returns_early_when_all_values_are_above_the_upper_bound(
        self,
    ):
        items = [
            SearchableItem(value=100),
            SearchableItem(value=300),
            SearchableItem(value=2),
            SearchableItem(value=500),
            SearchableItem(value=700),
            SearchableItem(value=900),
        ]
        result = find_population(target=0, tolerance=1, items=items)
        assert result is None

    def test_find_population_finds_optimal_set_when_inputs_contain_negative_numbers(
        self,
    ):
        target = 1000
        tolerance = 100
        values = [
            SearchableItem(value=-5),
            SearchableItem(value=-25),
            SearchableItem(value=1),
            SearchableItem(value=3),
            SearchableItem(value=5),
            SearchableItem(value=7),
            SearchableItem(value=9),
            SearchableItem(value=100),
            SearchableItem(value=300),
            SearchableItem(value=-250),
            SearchableItem(value=-125),
            SearchableItem(value=500),
            SearchableItem(value=405),
            SearchableItem(value=700),
            SearchableItem(value=900),
        ]
        result = find_population(target, tolerance, values)
        assert result.total_value == 925

        values = sorted([item.value for item in result.matched_items])
        assert values == [
            -250,
            -125,
            -25,
            -5,
            1,
            3,
            5,
            7,
            9,
            100,
            300,
            405,
            500,
        ]

    @pytest.mark.slow
    def test_find_population_finds_optimal_solution_in_range(self):
        values = [SearchableItem(value=value) for value in range(-1000, 5000)]
        target = 1000
        tolerance = 500

        result = find_population(target, tolerance, values)
        assert result.total_value == 1001
        expected_values = [val for val in range(-1000, 1002)]
        assert set([item.value for item in result.matched_items]) == set(
            expected_values
        )

    def test_returns_alternative_populations_if_requested(self):
        values = [
            SearchableItem(value=200),
            SearchableItem(value=100),
            SearchableItem(value=100),
        ]
        result, alternatives = find_population(
            target=200, tolerance=0, items=values, return_alternatives=True
        )
        assert result.matched_items == set(values[1:])
        assert len(alternatives) == 1
        assert alternatives[0].matched_items == set([values[0]])

    def test_find_population_finds_optimal_population_amid_noise_with_no_tolerance(
        self,
    ):
        target = 500
        tolerance = 0
        values = [
            SearchableItem(value=100),
            SearchableItem(value=100),
            SearchableItem(value=100),
            SearchableItem(value=100),
            SearchableItem(value=100),
            SearchableItem(value=-5),
            SearchableItem(value=-25),
            SearchableItem(value=1),
            SearchableItem(value=3),
            SearchableItem(value=5),
            SearchableItem(value=7),
            SearchableItem(value=9),
            SearchableItem(value=300),
            SearchableItem(value=-250),
            SearchableItem(value=-125),
            SearchableItem(value=500),
            SearchableItem(value=405),
            SearchableItem(value=700),
            SearchableItem(value=900),
        ]
        result = find_population(target, tolerance, values)
        assert result.matched_items == set(values[0:5])
        assert result.metadata["attempts_taken"] == 10

    def test_limits_subpopulation_searches_to_max_search_attempts(self, mocker):
        target = 500
        tolerance = 0
        values = [
            SearchableItem(value=100),
            SearchableItem(value=100),
            SearchableItem(value=100),
            SearchableItem(value=100),
            SearchableItem(value=100),
            SearchableItem(value=-5),
            SearchableItem(value=-25),
            SearchableItem(value=1),
            SearchableItem(value=3),
            SearchableItem(value=5),
            SearchableItem(value=7),
            SearchableItem(value=9),
            SearchableItem(value=300),
            SearchableItem(value=-250),
            SearchableItem(value=-125),
            SearchableItem(value=500),
            SearchableItem(value=405),
            SearchableItem(value=700),
            SearchableItem(value=900),
        ]

        mocker.patch(
            "groepszoeker.find_possible_sub_populations",
            mocker.MagicMock(return_value=[]),
        )
        find_population(target, tolerance, values, max_search_attempts=3)
        assert groepszoeker.find_possible_sub_populations.call_count == 3

    def test_find_population_finds_exact_set_with_no_tolerance_and_negative_numbers(
        self,
    ):
        items = [
            SearchableItem(value=-500),
            SearchableItem(value=-100),
            SearchableItem(value=-900),
            SearchableItem(value=-300),
            SearchableItem(value=-700),
        ]
        result = find_population(target=-900, tolerance=0, items=items)
        assert result.total_value == -900
        assert result.matched_items == set([items[0], items[1], items[3]])
        assert result.unmatched_items == set([items[2], items[-1]])

    def test_find_population_finds_exact_set_with_tolerance_and_negative_numbers(
        self,
    ):
        items = [
            SearchableItem(value=-500),
            SearchableItem(value=-100),
            SearchableItem(value=-900),
            SearchableItem(value=-500),
            SearchableItem(value=-700),
        ]
        result = find_population(target=-900, tolerance=100, items=items)
        assert result.total_value == -1000
        assert result.matched_items == set([items[0], items[3]])
        assert result.unmatched_items == set([items[1], items[2], items[-1]])

    def test_population_finds_exact_set_with_positive_target_and_negative_noise(
        self,
    ):
        items = [
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-100),
            SearchableItem(value=-100),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=100),
            SearchableItem(value=100),
        ]
        result = find_population(target=500, tolerance=0, items=items)
        assert result.total_value == 500
        assert result.matched_items == set(items[8:])

    def test_population_finds_exact_set_with_negative_target_and_positive_noise(
        self,
    ):
        items = [
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=100),
            SearchableItem(value=100),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-100),
            SearchableItem(value=-100),
        ]
        result = find_population(target=-500, tolerance=0, items=items)
        assert result.total_value == -500
        assert result.matched_items == set(items[8:])

    def test_population_finds_exact_set_with_negative_target_and_negative_noise(
        self,
    ):
        items = [
            SearchableItem(value=-100),
            SearchableItem(value=-100),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-100),
            SearchableItem(value=-100),
        ]
        result = find_population(target=-500, tolerance=0, items=items)
        assert result.total_value == -500

        items_with_value_of_neg_fifty = set(
            [item for item in items if item.value == -50]
        )
        assert all(
            [
                item in items_with_value_of_neg_fifty
                for item in result.matched_items
            ]
        )

    def test_population_finds_exact_set_with_positive_and_negative_noise(self):
        items = [
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=50),
            SearchableItem(value=100),
            SearchableItem(value=100),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-50),
            SearchableItem(value=-100),
            SearchableItem(value=-50),
            SearchableItem(value=900),
            SearchableItem(value=30),
            SearchableItem(value=25),
            SearchableItem(value=20),
            SearchableItem(value=10),
            SearchableItem(value=9),
            SearchableItem(value=1),
        ]
        result = find_population(target=500, tolerance=0, items=items)
        assert result.total_value == 500
        assert result.matched_items == set(items[0:8])


def test_classified_item_collector_classifies_by_value_grouping():
    values = [
        SearchableItem(value=1000),
        SearchableItem(value=99999),
        SearchableItem(value=1234581),
        SearchableItem(value=1),
        SearchableItem(value=-1),
        SearchableItem(value=-1000),
    ]
    classified_values = ClassifiedItemCollection(items=values)
    assert classified_values.units == set([values[3], values[4]])
    assert classified_values.hundreds == set()
    assert classified_values.thousands == set([values[0], values[-1]])
    assert classified_values.ten_thousands == set([values[1]])
    assert classified_values.millions == set([values[2]])


class TestCheckBounds:
    @pytest.mark.parametrize("total,target", [(101, 100), (-101, -100)])
    def test_check_returns_out_of_bounds_for_values_too_high_or_low_with_pos_and_neg_targets(
        self, total, target
    ):
        result = check_bounds(total, target, tolerance=0)
        assert result == TotalBoundsCheckStatus.out_of_bounds

    @pytest.mark.parametrize(
        "total,target,tolerance", [(110, 100, 9), (-110, -100, 9)]
    )
    def test_check_returns_out_of_bounds_for_out_of_bounds_values_with_tolerance(
        self, total, target, tolerance
    ):
        result = check_bounds(total, target, tolerance)
        assert result == TotalBoundsCheckStatus.out_of_bounds

    @pytest.mark.parametrize(
        "total,target,tolerance",
        [(99, 100, 0), (-99, -100, 0), (89, 100, 10), (-89, -100, 10)],
    )
    def test_check_bounds_returns_bounds_not_reached_if_total_below_lower_bound(
        self, total, target, tolerance
    ):
        result = check_bounds(total, target, tolerance)
        assert result == TotalBoundsCheckStatus.bounds_not_reached

    @pytest.mark.parametrize(
        "total,target,tolerance",
        [(95, 100, 10), (-95, -100, 10)],
    )
    def test_check_bounds_returns_in_bounds_if_value_is_within_tolerance_bounds(
        self, total, target, tolerance
    ):
        result = check_bounds(total, target, tolerance)
        assert result == TotalBoundsCheckStatus.in_bounds

    @pytest.mark.parametrize(
        "total,target,tolerance",
        [(110, 100, 10), (-110, -100, 10)],
    )
    def test_check_bounds_returns_at_outer_limit_if_value_is_at_outer_bound(
        self, total, target, tolerance
    ):
        result = check_bounds(total, target, tolerance)
        assert result == TotalBoundsCheckStatus.at_outer_limit

    @pytest.mark.parametrize(
        "total,target,tolerance",
        [(100, 100, 0), (-100, -100, 0)],
    )
    def test_check_bounds_returns_at_outer_limit_if_value_is_at_target_with_no_tolerance(
        self, total, target, tolerance
    ):
        result = check_bounds(total, target, tolerance)
        assert result == TotalBoundsCheckStatus.at_outer_limit
